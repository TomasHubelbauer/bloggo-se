# Sony Ericsson

> Sony Ericsson modding archeology

- SE *Sony Ericsson*
- CDA *Customization Data Article*
- DCU-60 - a Sony Ericsson proprietary cable
- Red color/certificate - a retail phone with highest security
- Brown color/certificate - a test phone with relaxed security
- Blue color/certificate - a development phone without security (no GDFS or IMEI)
- FS *File System*
- OTP *One-Time Programmable* memory
- GDFS *Global Data File System*
- IMEI *International Mobile Equipment Identity*
- AT *Trim Area* - same as GDFS but for S1
- ROM *Read-Only Memory*
- EROM *Extended Read-Only Memory*
- SEMCBOOT - EROM
- FAR - FAR Manager
- Patch
- Certificate
- Omnius
- OTP CID
- EROM CID
- SEFP Sony Ericsson Flash Plugin
- SEUS Sony Ericsson Update Service (provides SSWs?)
- P1 protocol - for flashing XPeria devices
- SEMC protocol - for flashing DBXXX devices
- BEFG Binary 
- AT AT commands
- [ ] To do
- [ ] http://www.omnius-server.com/glossary


## Platforms, chipsets etc.

- Chipset - a set of chips on the phones' logic board
- Platform - a group of chipsets
- EMP *Ericsson Mobile Platform* chipset
- A0 - a platform with the DB1### chipset
- A1 - a platform with the DB2### chipset
- A2 - a platform with the DB3### chipset
- S1 - a non-EMP platform with the Locosto chipset
- S2 - a non-EMP platform with the Neptune chipset
- DB2000
- DB2010
- DB2012
- DB2020
- DB3150
- DB3350
- CID *Caller/Customer ID*
- CID36
- CID49
- CID50
- CID51
- CID52
- CID53
- CID80
- CID81
- PNX5230

## File Formats

- MBN - main firmware (contains software), always unencrypted?
- FBN - flash file system (contains files), always unencrypted?
- SSW - SE firmware (0xBABE first 4 bytes, then CID, hash etc) encrypted or unencrypted
- BIN same category as MBN, FBN, SSW
- RAW - unencrypted FBN
- VKP - patch
- REST - ?
- BABE
- ELF - Elf program
- THM - theme - is a TAR
- SIS - Symbian app installer file format
- UTZ - some sort of a Symbian theme file, is actually a ZIP file
- JAR - Java archive file format

## Tools

- XS++
- SETool2Lite
- JDFlasher
- BFlash?
- FAR Manager
- OFlash
- mkrest
- SEFP Sony Ericsson Flash Plugin
- main2raw.exe
- SEFStool4.exe
- A2 Uploader
- A2 tool (=A2 uploader)
- SEtool
- Sonics
- DaVinci

## People

- den_po http://justdanpo.ru https://github.com/justdanpo
- Sivin
- KRtek
- etc.
- Darkmen (SEFP2)

## Firmwares

- 

## Bits

- `main2raw .fbn 2020`
- `sefstool4 .raw 2020`

## Links

- [se-tuning.cs](http://www.se-tuning.cz)
- iProTebe
- Se-nse
- Esato
- [ ] To do
- [ ] Find the presentation with the timeline and stuff
- https://fms.komkon.org/EMUL8/HOWTO.html
- http://www.topsony.com/forum/cmps_index.php
- https://www.nettwerked.net/SE-DB-2020-FM.txt
